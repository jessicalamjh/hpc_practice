#!/bin/bash -l
#SBATCH --job-name="cpi_mpi"
#SBATCH --time=00:10:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=2
#SBATCH --ntasks-per-node=36
#SBATCH --cpus-per-task=1
#SBATCH --partition=debug
#SBATCH --constraint=mc
#SBATCH --hint=nomultithread
#SBATCH --account=uzh8
#SBATCH --output=output_cpi_mpi.log

srun cpi_mpi
