#!/bin/bash -l
#SBATCH --job-name="cpi_omp"
#SBATCH --time=00:10:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=36
#SBATCH --partition=debug
#SBATCH --constraint=mc
#SBATCH --hint=nomultithread
#SBATCH --account=uzh8
#SBATCH --output=output_cpi_omp.log

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

srun cpi_omp
